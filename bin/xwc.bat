@echo off

SET PASSTHROUGH=1

IF "%XWC_HOME%" == "" (
 echo Please set the XWC_HOME environment variable to your vagrant directory.
 pause
) ELSE (
 CD %XWC_HOME%

 IF "%1" == "start" (
  SET PASSTHROUGH=0
  echo Starting Rails 4.2.4 Vagrant VM.
  vagrant up
 )

 IF "%1" == "stop" (
  SET PASSTHROUGH=0
  vagrant ssh -c "sudo shutdown -h now"
 )

 IF "%1" == "new" ( 
  SET PASSTHROUGH=0
  echo Creating new Rails project %2...
  vagrant ssh -c "/vagrant/bin/xwcd new %2 %3 %4 %5 %6 %7 %8 %9"  
 )

 IF "%1" == "list" (
  SET PASSTHROUGH=0
  dir projects/
 )
 IF "%1" == "status" (
  SET PASSTHROUGH=0
  vagrant status
 )
 IF "%2" == "start" (
  SET PASSTHROUGH=0
  echo Starting rails server on port 3000 for %1
  vagrant ssh -c "/vagrant/bin/xwcd %1 start"
 )
 IF "%2" == "stop" (
  SET PASSTHROUGH=0
  echo Starting rails server on port 3000 for %1
  vagrant ssh -c "/vagrant/bin/xwcd %1 stop"
 )
 
 IF %PASSTHROUGH%==1 (
  echo Running command %2 %3 %4 %5 %6 %7 %8 %9 in project '%1'
  vagrant ssh -c "/vagrant/bin/xwcd %1 %2 %3 %4 %5 %6 %7 %8 %9"
 )
)
