Rails 4.2.4 Vagrant VM
======================

This virtual machine includes:

  - MySql 5.5
  - Postgresql 9.3
  - Webmin 1.76
  - Ruby 2.2.3
  - Rails 4.2.4


Getting Started/Setup
=====================

Clone the repository and run 'vagrant up'

Create an environment variable called XWC_HOME
and point it to the directory you cloned.

Add $XWC_HOME/bin to your path


Starting/Stopping the VM
=========================

```
#!python


xwc start
xwc stop
```



Common XWC commands
===================

```
#!python


xwc new [some ruby project]
xwc list
xwc myapp bundle install
xwc myapp bundle install --without production
xwc myapp rake db:migrate
xwc myapp start
xwc myapp stop
```

USING WEBMIN
============

Open a browser and navigate to https://localhost:10000/

Log in using - user: vagrant, password: vagrant

You can manage/create your databases here, add users, and assign permissions to them.

The MySQL root database password is also, 'vagrant'.

TO DO
=====

- Add a corresponding xwc.bat for Windows machines.
- Test remote debugging using rdebug-ide

    https://confluence.jetbrains.com/display/RUBYDEV/How+to+setup+and+run+ruby+remote+debug+session

- Add a task to tail logs from the guest OS
