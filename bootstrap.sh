#!/usr/bin/env bash

echo 'Updating apt repositories'
add-apt-repository ppa:chris-lea/node.js

sh -c "echo 'deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main' > /etc/apt/sources.list.d/pgdg.list"
wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -

wget -q http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -

echo 'deb http://download.webmin.com/download/repository sarge contrib' > /etc/apt/sources.list.d/webmin.list
echo 'deb http://webmin.mirror.somersettechsolutions.co.uk/repository sarge contrib' >> /etc/apt/sources.list.d/webmin.list

apt-add-repository -y ppa:brightbox/ruby-ng

apt-get update

echo 'Installing Ruby dependencies'
apt-get install -y git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev

apt-get install -y libgdbm-dev libncurses5-dev automake libtool bison libffi-dev

echo 'Installing NodeJS'
apt-get install -y nodejs

echo 'Installing MySql 5.5'
debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'
apt-get install -y mysql-server mysql-client libmysqlclient-dev
apt-get install -f

echo 'Installing PostGreSql 9.3'
apt-get install -y postgresql-common
apt-get install -y postgresql-9.3 libpq-dev

echo 'Installing Webmin'
apt-get install -y webmin

apt-get autoremove -y

echo 'Installing Rails 4.2.4' 
apt-get install -y Ruby ruby2.2 ruby2.2-dev
update-alternatives --set ruby /usr/bin/ruby2.2 
update-alternatives --set gem /usr/bin/gem2.2

gem install bundler -N
gem install ruby-debug-ide -N
gem install rails -v 4.2.4
gem install rvm
